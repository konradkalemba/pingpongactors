import akka.actor.{ ActorSystem, Props }

object Main {   
  def main(args: Array[String]){
    val system = ActorSystem()
    val player1 = system.actorOf(Props[Player])
    val player2 = system.actorOf(Props[Player])

    player1 ! Player.Start(player2, 6)
  }
}
