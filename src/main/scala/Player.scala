import akka.actor.{ Actor, ActorRef }

object Player {
  case class Start(opponent: ActorRef, hits: Int)
  case object Stop

  private case class Ball(var message: String, var hits: Int) {
    def this(hits: Int) {
      this("ping", hits)
    }

    def reflect = {
      message = if (message == "ping") "pong" else "ping"
      hits -= 1

      this
    }
  }
}

class Player extends Actor { 
  import Player._

  def receive() = {
    case Start(opponent, hits) => send(opponent, new Ball(hits))
    case ball: Ball => send(sender, ball.reflect)
    case Stop => stop
    case _ => println("Wrong request")
  }

  private def send(opponent: ActorRef, ball: Ball) = {
    if (ball.hits > 0) {
      println("%s: %s".format(self.path.name, ball.message))
      
      opponent ! ball
    } else {
      stop
      opponent ! Stop
    }
  }

  private def stop() = {
    context.stop(self)
    println("%s: stopped".format(self.path.name))
  }
}
